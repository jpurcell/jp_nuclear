{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <center> Similarity Renormalization Group Flow </center>\n",
    "\n",
    "<center>By Jordan Purcell </center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://inspirehep.net/record/1093277/files/tiled_s_3S1_kvnn06_reg_0_3_0_ksq.png\" width=\"100%\">\n",
    "<p style=\"text-align: right;\">Image from:http://inspirehep.net/record/1093277/plots</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Authors\n",
    "\n",
    "Heiko Hergert, Scott Bogner"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Abstract\n",
    "\n",
    "In general, the matrices associated with quantum mechanical operators are not diagonal, and so the values of physical relevance are not immediately clear from an inspection of matrix elements. This is ameliorated by applying a unitary transformation to the matrix under consideration - this is a transformation that renders the original matrix as a diagonal matrix without compromising the physical outcome of applying the measurement to the state. The Similarity Renormalization Group (SRG) is one such unitary transformation; it is a continuous unitary transformation U(s) parameterized by a variable s that can run from 0 to infinity. As s grows larger and larger, the off-diagonal terms become increasingly suppressed to 0, while the original diagonal terms evolve to become what they will when the matrix is completely diagonalized. For a sufficiently large s, one can get a very good estimate for ground state energies in systems of many (n>3) particles - these are experimentally testable benchmarks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Statement of Need\n",
    "\n",
    "The strong-interaction sector of the Standard Model is provided by Quantum Chromodynamics (QCD), but the description of nuclear observables on the level of quarks and gluons is not feasible, except in the lightest few-nucleon systems. Issues arise because of the complexity of the strong interaction compared to other fundamental interactions (like electricity/magnetism) - as a result, we will need another theoretical tool to begin carving a predictive path forward in this landscape. There exists a number of effective theories for describing different regimes of QCD at different scales of energy, and SRG provides a systematic and rigorous way of \"dialing\" through these different scales, and lends predictive capacity for a someone interested in conducting experiments at a given energy. Most importantly, SRG show how different effective theories emerge and relate to one another."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Installation instructions\n",
    "Requires Python 2 (will NOT work with Python 3) - execute srg_pairing or imsrg_pairing script from terminal"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Tests\n",
    "This program was tested using an exactly solvable model called the pairing model, and the script included outputs plots indicating how SRG solutions compare to the exact solution.\n",
    "\n",
    "Within the script is a function named Hamiltonian on line 149, and here the pairing model is explicitly coded. For different models, one would simply input their own Hamiltonian in place of the pairing model. Assuming that the provided Hamiltonian is analytically solvable, the program as written will solve the Hamiltonian exactly before applying SRG method. At the end, a plot comparing the exact solution to the SRG solution is furnished.\n",
    "\n",
    "The output can be modified in terms of energy level spacings and coupling parameters (delta and g, respectively) in the main body of the code (line 198 and 199), and one can select for solutions at different scales of SRG flow by adjusting the values in flowparams on line 211."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Example usage\n",
    "The Pairing model actually represents a very good approximation to some of the salient details of the strong interaction, and so the use of SRG on this model shows preliminary utility. In principle, this algorithm could be used as-is for more complicated Hamiltonian forms, such as the Coulomb gauge QCD Hamiltonian; what would be required would be substantial computational resources. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Methodology\n",
    "\n",
    "The final project lined up with my intention to understand how to implement SRG to solve for the eigenvalues associated with the pairing model of a given coupling strength; I fell short of being able to actually implement the Magnus Expansion. Many of the terms used in the expansion are defined recursively, and knowing when/how to implement each iteration in the recursion to solving an ODE escaped my efforts. If I had more time to focus on just this work, I could have definitely implemented this.\n",
    "\n",
    "The bulk of my work was in researching the mathematical machinery of SRG, how it works in the abstract on general matrices, and then how we implement this into a computational language. I was successfully able to use SRG on the pairing model, and showed that it does yield the exact known energies in the limit as s grows large. Though not exhaustive by an means, I was able to construct my own (analytically diagonalizable) Hermitian matrices, and was able to confirm that the SRG flows the matrices to the correct diagonal form."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Concluding Remarks\n",
    "\n",
    "While I did not get to explicitly implement all of the theory that I learned, I did in fact learn a substantial amount of many-body theory. The reason for my lack of implementation is in how advanced the material I was trying to learn was. I learned how to symbolitcally represent the states of systems of many particles in quantum mechanics, and learned how to expand the formalism to account for the interactions between large numbers of particles leading to dynamics and changes of state - this was the bulk of my research. Once I established the theory and learned how to read what kinds of effective theories were in the literature, I could begin to piece together the narrative of similarity renormalization group, and why such a tool is now necessary to make progress. \n",
    "\n",
    "As someone considering a dual in Physics and CMSE, I would say that this project substantially helped me to reach my goals. SRG is a young tool of the trade, and there are burgeoning aspects of it that could be entire PhD's themselves - like parallelizing the code to make it suitable for a system like the HPCC. \n",
    "\n",
    "In the near future, I intend to work with my group to implement the magnus expansion for SRG, and eventually to expand it to IMSRG. Further into the future, I could potentially see myself approaching a parallelization project."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Mentions\n",
    "\n",
    "\"Nuclear Structure from the In-Medium Similarity Renormalization Group\" - https://arxiv.org/abs/1805.09221"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# References\n",
    "The majority of material pertaining to SRG can be found in this book:\n",
    "\"An Advanced Course in Computational Nuclear Physics - 2017\" - DOI:10.1007/978-3-319-53336-0\n",
    "\n",
    "I utilized this paper for studies in Magnus Expansion:\n",
    "\"The Magnus expansion and some of its applications\" - https://arxiv.org/pdf/0810.5488.pdf"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
