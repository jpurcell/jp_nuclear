{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <center>Using Ordinary Differential Equations (ODEs) in Theoretical Nuclear Physics</center>\n",
    "\n",
    "<center>by Jordan Purcell</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "ODE's are widely used in theoretical nuclear physics, and at a variety of different scales of energy and particle number. In classical physics, an enormous variety of trajectory solutions for position are solved using ODE's - for example, the position of the end of a pendulum as a function of time and an angle $\\theta$ is given by:\n",
    "$\\hspace{1cm}$\n",
    "<center> $ \\ddot{\\theta} = -k\\sin (\\theta) $ <center>\n",
    "\n",
    "In the modern context of quantum mechanics, the most common application of ODEs is in solving the second-order ODE that is the Schrödinger equation:\n",
    "$\\hspace{1cm}$\n",
    "<center> $ \\frac{\\partial^2}{\\partial\\ x^2}|\\psi \\rangle = -\\frac{2m}{\\hbar^2}[E-V(x)]|\\psi \\rangle $ <center>\n",
    "\n",
    "This equation allows for a description of the observable states and values of a single quantum particle in terms of the potential parameter V(x) describing the environment that the particle is interacting with. In the research that I intend to conduct, we consider systems of particles with number N >> 1, and in some cases as $ N \\rightarrow \\infty $. In such systems, the single-particle $|\\psi \\rangle$ state vector can't simply describe the state of one particle, but must be able to simultaneously account for the states of all N particles in an N-particle state given by:\n",
    "$\\hspace{1cm}$\n",
    "<center> $ |\\psi \\rangle \\rightarrow |\\psi _1 \\psi _2 ... \\psi _n) = |\\psi _1 \\rangle \\otimes |\\psi _2 \\rangle \\otimes ... \\otimes |\\psi _n \\rangle $\n",
    "    \n",
    "Where each $|\\psi _i \\rangle$ is the single-particle state associated only with the i'th particle in the ensemble [1]. In practice, an operator like H can be represented as a matrix, and $|\\psi \\rangle$ can be represented as a vector. Just as a matrix transforms a vector into another vector, an operator acting on a quantum state $|\\psi \\rangle$ will produce another quantum state. Our state vectors can be expressed in any basis that could be relevant to the physical system under consideration, and in each of these bases we find our representation using the completeness relation\n",
    "$\\hspace{1cm}$\n",
    "<center> $\\mathbb{1}|\\psi \\rangle = \\sum \\limits _{i}|i \\rangle \\langle i|\\psi \\rangle$ <center>\n",
    "\n",
    "The basis that we choose to represent our single particle states in will determine the form that we represent our matrix H in - specifically, it will determine what the elements $H _{ij}$ are through the use of completeness:\n",
    "$\\hspace{1cm}$\n",
    "<center> $\\mathbb{1 _i}H\\mathbb{1 _j} = \\sum \\limits _{i} \\sum \\limits _{j}|i \\rangle \\langle i|H|j \\rangle \\langle j|$ <center>\n",
    "    \n",
    "In certain bases, the elements of this matrix can possibly be diagonal. This is a desirable property of a matrix transformation, because it implies that the action of the matrix on an input vector is simply to scale the components of the vector by the associated elements (eigenvalues) of this diagonal matrix. Paramount to this is the fact that we associate operators with physical observables, the eigenvectors of the matrices associated with these operators are the allowed quantum states, and the eigenvalues are the results of measuring the quantity associated with the operator when the system is in a particular eigenstate. \n",
    "$\\hspace{1cm}$\n",
    "\n",
    "When a matrix is not diagonal, the effect is to also induce a rotation of the input vectors, forcing us to consider how off-diagonal elements of the vector are also coupling to and scaling the components of our input vector. This situation is much more computationally intensive, and since the dimension of the matrices relevant for many-body physics can be in the millions, it can quickly become an intractable problem. Thankfully through the theory of linear algebra, there is a way of finding what our basis must be to resolve diagonal matrices, and what those diagonal elements must be through a process called diagonalization. Starting with a non-diagonal matrix $M$, we produce a diagonal matrix $H$ by acting on $M$ with two new unitary matrices $U$ and $U^\\dagger$, where $U^\\dagger$ is the complex transpose of $U$. Unitarity means that $ UU^\\dagger = \\mathbb{1}$ so that the resulting matrix $H$ maintains the same number of orthogonal basis components, and that the lengths of any vector in the original representation are not scaled in the new representation. The unitary transformation of $M \\rightarrow H$ is achieved through:\n",
    "\n",
    "<center> $UMU^\\dagger = H$ <center>\n",
    "    \n",
    "In the past 30 years, there have been strategies for defining this unitary transformation to render high-dimensional problems tractable. The aim has been the prediction of observable quantities such as ground-state energies of many-body quantum systems, like plutonium nuclei or even neutron stars. One of these methods is going to be the focus of my project, Similarity Renormalization Group (SRG) [2]. The idea behind SRG is to consider a unitary transformation of an initial non-diagonal matrix $H _0$ parameterized by a continuous parameter s:\n",
    "$\\hspace{1cm}$\n",
    "<center> $H(s) = U(s)H _0U(s)^\\dagger$ Here we define $H(s \\rightarrow 0) = H _0$ and $H(s \\rightarrow \\infty) = H _{diagonal}$ <center>\n",
    "    \n",
    "By taking a derivative $\\frac{d}{ds}H(s)$ and applying it to the right side of the above-defined equation, we eventually arive at an ODE:\n",
    "$\\hspace{1cm}$\n",
    "<center> $\\frac{d}{ds}H(s) = [\\eta(s), H(s)]$ where $\\eta(s) \\equiv \\frac{dU(s)}{ds}U(s)^\\dagger$ <center>\n",
    "\n",
    "This is a first order ODE known as the SRG flow equation for the Hamiltonian $H(s)$. The SRG flow equation [3]  describes the evolution of our unitarily transformed Hamiltonian under the action of a dynamical generator of transformations $\\eta(s)$. The utility of this construction is that it can be integrated numerically without explicitly constructing the unitary transformation itself. The variation in s is a variation in \"scale\", specifically in the energy/momentum scale of the physical system, and allows us to suppress divergent terms due to local interactions so that we are solving for the energies of a new \"effective\" system that is self-similar to the original system."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# References\n",
    "\n",
    "[1] - Dickhoff, Van Neck (2008), Many-Body Theory Exposed: Propagator Description of Quantum Mechanics in Many-Body         Systems, World Scientific Publishing Company, Ch. 1\n",
    "\n",
    "[2] - arXiv:hep-ph/0009071\n",
    "\n",
    "[3] - Hjorth-Jensen, Morten & Lombardo, Maria & van Kolck, U.. (2017). An Advanced Course in Computational Nuclear         Physics: Bridging the Scales from Quarks to Neutron Stars. 10.1007/978-3-319-53336-0. "
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
